import pickle


def print_hello():
    print("Hello docker image...")
    with open('/sharedFile/sample_docker_file.data', 'wb') as file:
        # store the data as binary data stream
        pickle.dump("hello", file)


if __name__ == '__main__':
    print_hello()
